# Generated by Django 2.1.7 on 2019-05-06 00:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('django_out_auth', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='outauthuser',
            name='out_token',
            field=models.CharField(blank=True, default='', max_length=512, verbose_name='Out Token'),
        ),
        migrations.AlterUniqueTogether(
            name='outauthuser',
            unique_together={('auth_app_name', 'out_id')},
        ),
    ]
