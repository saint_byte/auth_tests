from django.shortcuts import render, redirect
from .utils import get_additional_apps
from .models import OutAuthUser
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.http import HttpResponse
from .exceptions import NotSetSettingException
import importlib
import traceback
import sys
from django.conf import settings
from django.urls import reverse
import uuid


def index(request):
    ctx = {}
    ctx['additional_apps'] = get_additional_apps()
    return render(request, "django_out_auth/index.html", ctx)


def first_action(request, additional_app='', app_postfix=''):
    request.session['outauth_back'] = request.GET.get('back', '/')
    try:
        module = importlib.import_module('django_out_auth_{}.views'.format(app_postfix))
        first_action_function = getattr(module, 'first_action', None)
        if first_action_function is None:
            return HttpResponse(""" first_action function not found in app """)
        return first_action_function(request, additional_app, app_postfix)
    except ImportError as e:
        return HttpResponse('can\'t load django_out_auth_{}.views'.format(app_postfix))
    except NotSetSettingException as e:
        return HttpResponse(e.message)
    except:
        if settings.DEBUG:
            ex = sys.exc_info()
            tbs = traceback.format_exc()
            return HttpResponse(str(ex) + '<pre>' + str(tbs) + '</pre>')
        return HttpResponse("Something wrong in additional_app first_action ")
    return HttpResponse("Its impossble")


def second_action(request, additional_app='', app_postfix=''):
    try:
        module = importlib.import_module('django_out_auth_{}.views'.format(app_postfix))
    except:
        return HttpResponse('can\'t load django_out_auth_{}.views'.format(app_postfix))
    try:
        second_action_function = getattr(module, 'second_action')
    except:
        return HttpResponse(""" second_action function not found in app """)
    try:
        result = second_action_function(request, additional_app, app_postfix)
        if isinstance(result, HttpResponse):
            return result
        (out_id, token, first_name, last_name, email) = result
        try:
            OutUser = OutAuthUser.objects.get(auth_app_name=additional_app, out_id=out_id)
            OutUser.token = token
            OutUser.save()
        except OutAuthUser.DoesNotExist:
            if not hasattr(settings, 'DJANGO_OUT_AUTH_CREATE_USER'):
                return redirect(reverse('django_out_auth_{}_error_action'.format(app_postfix)))
            if settings.DJANGO_OUT_AUTH_CREATE_USER:
                user = User()
                user.username = "{}-{}".format(app_postfix, str(uuid.uuid4()))
                user.first_name = first_name
                user.last_name = last_name
                user.email = email
                user.is_staff = False
                user.is_active = True
                user.set_password(str(uuid.uuid1()))
                user.save()
                OutUser = OutAuthUser(django_user=user, auth_app_name=additional_app, out_id=out_id, out_token=token)
                OutUser.save()
                if hasattr(settings, 'DJANGO_OUT_AUTH_CREATE_USER_CALLBACK'):
                    pass
    except NotSetSettingException as e:
        return HttpResponse(str(e.message))
    except:
        if settings.DEBUG:
            ex = sys.exc_info()
            tbs = traceback.format_exc()
            return HttpResponse(str(ex) + '<pre>' + str(tbs) + '</pre>')
        return HttpResponse("Something wrong in additional_app second_action")
    # Here  OutUser ready in any case
    if hasattr(settings, 'DJANGO_OUT_AUTH_BEFORE_AUTH_CALLBACK'):
        pass
    login(request, OutUser.django_user)
    if hasattr(settings, 'DJANGO_OUT_AUTH_AFTER_AUTH_CALLBACK'):
        pass
    return redirect(request.session.get('outauth_back', '/'))


def error_action(request, additional_app='', app_postfix=''):
    ctx = {}
    ctx['additional_app'] = additional_app
    ctx['app_postfix'] = app_postfix
    return render(request, "django_out_auth/error.html", ctx)
