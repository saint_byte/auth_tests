__author__ = 'sb'


class NotSetSettingException(Exception):
    def __init__(self, setting):
        self.message = "Not set {} in setting.py".format(setting)


class WebProblemException(Exception):
    def __init__(self, method, url):
        self.message = "Cant {} {}".format(method, url)