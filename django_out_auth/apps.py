from django.apps import AppConfig


class DjangoOutAuthConfig(AppConfig):
    name = 'django_out_auth'
