__author__ = 'sb'
from django.conf import settings
import urllib.request
from .exceptions import WebProblemException


def get_additional_apps(flat=False):
    """
    Get list of additional

    :param flat: return as flat list ?
    :return: list or list of sequence
    """
    itsme = 'django_out_auth'
    app_prefix = itsme + "_"

    result = []

    for app in settings.INSTALLED_APPS:
        if app == itsme:
            continue
        if app.startswith(app_prefix):
            if flat:
                result.append(app)
            else:
                result.append((app.replace(app_prefix, ''), app), )
    return result


def get_site_uri(request):
    """
    Return some like http://site.com
    If u want override default benahoiur set SITE in settings.py

    :param request:
    :return:
    """
    from django.conf import settings
    if getattr(settings, 'SITE', False):
        return settings.SITE
    return '{scheme}://{host}'.format(scheme=request.scheme, host=request.get_host())


def get_data(url):
    response = urllib.request.urlopen(url)
    data = response.read()
    encoding = response.info().get_content_charset('utf-8')
    return data.decode(encoding)
