__author__ = 'sb'

from django.conf import settings
from django.urls import path

from . import views
from .utils import get_additional_apps

urlpatterns = [
    path("", views.index, name="django_out_auth_index_action")
]
additional_apps = get_additional_apps()
for app in additional_apps:
    urlpatterns += [
        path(app[0] + "/1/", views.first_action,
             name="django_out_auth_{}_first_action".format(app[0]),
             kwargs={'additional_app': app[1], 'app_postfix': app[0]}),
        path(app[0] + "/2/", views.second_action,
             name="django_out_auth_{}_second_action".format(app[0]),
             kwargs={'additional_app': app[1], 'app_postfix': app[0]}),
        path(app[0] + "/error/", views.error_action,
             name="django_out_auth_{}_error_action".format(app[0]),
             kwargs={'additional_app': app[1], 'app_postfix': app[0]}),
    ]

