from django.contrib import admin
from .models import OutAuthUser


class OutAuthUserAdmin(admin.ModelAdmin):
    list_display = ['django_user', 'auth_app_name', 'out_id']


admin.site.register(OutAuthUser, OutAuthUserAdmin)
