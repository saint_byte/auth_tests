from django.db import models
from django.contrib.auth.models import User
from django.conf import settings

class OutAuthUser(models.Model):
    django_user = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='outauth2user',
                                    verbose_name="Django User")
    auth_app_name = models.CharField(max_length=128,default="",verbose_name="OutAuth app name")
    out_id = models.CharField(max_length=512, default="", verbose_name="Out ID")
    out_token = models.CharField(max_length=512, default="", blank=True, verbose_name="Out Token")

    def __str__(self):
        return str(self.django_user)

    def has_add_permission(self,request):
        if hasattr(settings,'DJANGO_OUT_AUTH_MANUAL_CREATE_USER'):
            if settings.DJANGO_OUT_AUTH_MANUAL_CREATE_USER:
                return True
        return False

    class Meta:
        verbose_name="Out Auth to Django Users"
        verbose_name_plural="Out Auth to Django Users"
        unique_together = [['auth_app_name', 'out_id'],]


