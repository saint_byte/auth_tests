from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
def index(request):
    ctx = {}
    return render(request, "index.html", ctx)

def logout_view(request):
    logout(request)
    return redirect('/')

def test1(request):
    from django_out_auth.utils import get_site_uri, get_additional_apps
    ctx = {}

    ctx['get_site_uri'] = get_site_uri(request)
    ctx['additional_apps'] = get_additional_apps()
    return render(request, "test1.html", ctx)

def test2(request):
    ctx = {}
    return render(request, "test2.html", ctx)

def useragreement(request):
    ctx = {}
    return render(request, "useragreement.html", ctx)

def privacypolicy(request):
    ctx = {}
    return render(request, "privacypolicy.html", ctx)