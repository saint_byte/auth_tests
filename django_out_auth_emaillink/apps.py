from django.apps import AppConfig


class DjangoOutAuthEmaillinkConfig(AppConfig):
    name = 'django_out_auth_emaillink'
