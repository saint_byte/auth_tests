from django.apps import AppConfig


class DjangoOutAuthDummyConfig(AppConfig):
    name = 'django_out_auth_dummy'
