from django.shortcuts import render, redirect
from django.conf import settings
from django.urls import reverse
from django_out_auth.exceptions import NotSetSettingException
from django_out_auth.utils import get_site_uri, get_data
import json
from django.http import HttpResponse

FACEBOOK_API_VERSION = 'v3.3'


def first_action(request, additional_apps='', app_postfix=''):
    if not hasattr(settings, 'DJANGO_OUT_AUTH_FACEBOOK_KEY'):
        raise NotSetSettingException('DJANGO_OUT_AUTH_FACEBOOK_KEY')
    if not hasattr(settings, 'DJANGO_OUT_AUTH_FACEBOOK_SECRET'):
        raise NotSetSettingException('DJANGO_OUT_AUTH_FACEBOOK_SECRET')
    url = 'https://www.facebook.com/{}/dialog/oauth?client_id={}&redirect_uri={}&scope={}&response_type={}'.format(
        FACEBOOK_API_VERSION,
        settings.DJANGO_OUT_AUTH_FACEBOOK_KEY,
        get_site_uri(request) + reverse('django_out_auth_{}_second_action'.format(app_postfix)),
        'public_profile',
        'code')
    return redirect(url)


def second_action(request, additional_apps='', app_postfix=''):
    if request.GET.get('error',False):
        return redirect(reverse('django_out_auth_{}_error_action'.format(app_postfix)))
    url = 'https://graph.facebook.com/{}/oauth/access_token?client_id={}&redirect_uri={}&client_secret={}&code={}'.format(
        FACEBOOK_API_VERSION,
        settings.DJANGO_OUT_AUTH_FACEBOOK_KEY,
        get_site_uri(request) + reverse('django_out_auth_{}_second_action'.format(app_postfix)),
        settings.DJANGO_OUT_AUTH_FACEBOOK_SECRET,
        request.GET.get('code', '')
    )

    data = get_data(url)
    data_arr = json.loads(data)
    access_token = data_arr.get('access_token', '')
    url = 'https://graph.facebook.com/{}/me?access_token={}&fields={}'.format(
        FACEBOOK_API_VERSION,
        access_token,
        'id,first_name,last_name'
    )
    data = get_data(url)
    data_arr = json.loads(data)
    email = "{}@{}.com".format(data_arr['id'],app_postfix) #Hide email in any case
    return (data_arr['id'], access_token, data_arr['first_name'],data_arr['last_name'],email)


def error_action(request, additional_apps='', app_postfix=''):
    ctx = {}
    return render(request, "django_out_auth_facebook/error.html", ctx)
