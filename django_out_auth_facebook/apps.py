from django.apps import AppConfig


class DjangoOutAuthFacebookConfig(AppConfig):
    name = 'django_out_auth_facebook'
