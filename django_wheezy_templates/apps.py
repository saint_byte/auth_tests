from django.apps import AppConfig


class DjangoWheezyTemplatesConfig(AppConfig):
    name = 'django_wheezy_templates'
