from django.shortcuts import render, redirect
from django.conf import settings
from django.urls import reverse
from django_out_auth.exceptions import NotSetSettingException
from django_out_auth.utils import get_site_uri
import urllib, urllib.request
import json
from django.http import HttpResponse


def first_action(request, additional_apps='', app_postfix=''):
    if not hasattr(settings, 'DJANGO_OUT_AUTH_GOOGLE_OAUTH2_KEY'):
        raise NotSetSettingException('DJANGO_OUT_AUTH_GOOGLE_OAUTH2_KEY')
    if not hasattr(settings, 'DJANGO_OUT_AUTH_GOOGLE_OAUTH2_SECRET'):
        raise NotSetSettingException('DJANGO_OUT_AUTH_GOOGLE_OAUTH2_SECRET')
    url = 'https://accounts.google.com/o/oauth2/v2/auth?client_id={}&scope={}&redirect_uri={}&response_type=code'.format(
        settings.DJANGO_OUT_AUTH_GOOGLE_OAUTH2_KEY,
        'profile',
        get_site_uri(request) + reverse('django_out_auth_{}_second_action'.format(app_postfix))
    )
    return redirect(url)


def second_action(request, additional_apps='', app_postfix=''):
    # https://developers.google.com/identity/protocols/OAuth2WebServer#handlingresponse
    if request.GET.get('error', False):
        return redirect(reverse('django_out_auth_{}_error_action'.format(app_postfix)))
    url = 'https://www.googleapis.com/oauth2/v4/token'
    data = {
        'code': request.GET.get('code', ''),
        'client_id': settings.DJANGO_OUT_AUTH_GOOGLE_OAUTH2_KEY,
        'client_secret': settings.DJANGO_OUT_AUTH_GOOGLE_OAUTH2_SECRET,
        'redirect_uri': get_site_uri(request) + reverse('django_out_auth_{}_second_action'.format(app_postfix)),
        'grant_type': 'authorization_code'
    }
    data = urllib.parse.urlencode(data).encode()
    req = urllib.request.Request(url, data=data)
    response = urllib.request.urlopen(req)
    result = response.read()
    encoding = response.info().get_content_charset('utf-8')
    data_arr = json.loads(result.decode(encoding))
    access_token = data_arr['access_token']
    url = 'https://www.googleapis.com/plus/v1/people/me'
    headers = {
        'Authorization': 'Bearer {}'.format(access_token)
    }
    response = urllib.request.urlopen(urllib.request.Request(url, headers=headers))
    result = response.read()
    encoding = response.info().get_content_charset('utf-8')
    data_arr = json.loads(result.decode(encoding))
    email = "{}@{}.com".format(data_arr['id'], app_postfix)  # Hide email in any case
    return (data_arr['id'], access_token, data_arr['name']['givenName'], data_arr['name']['familyName'], email)
