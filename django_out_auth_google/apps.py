from django.apps import AppConfig


class DjangoOutAuthGoogleConfig(AppConfig):
    name = 'django_out_auth_google'
